![logo-splash](https://bytebucket.org/ng-plex/dts-project-assets/raw/dc4b77d942e62e0ef4b33d6962e4b7aab28ff008/assets/mobile-app/logo.jpg?token=00b57b80def211adc2ba15a7843e61912c1ee1ac)

# Descripción

Prototipo y aplicación móvil para S.O Android destinadas a la comunicación y seguimiento de incidencias en tiempo real de cualquier ámbito espacial.

# Casos de Uso

Esta aplicación podrá ser incorporada a modo de Sistema de Gestión de Incidencias que requiera de la interacción del Usuario general para comunicar cualquier incidencia en vía pública a la Entidad Gestora que así lo requiera. Entre las aplicaciones posibles se han tenido en cuenta:

- Servicios operativos de cualquier Entidad Gestora local.
- Servicios de Mantenimiento de la vía pública.
- Servicios de Parques y Jardines.
- Empresas de Mantenimiento.
- Administraciones Públicas y sus departamentos encargados de la gestión del patrimonio.
- etc.

# Tecnología utilizada

A través de la imagen que se muestra a continuación se puede apreciar la tecnología utilizada para el desarrollo de esta aplicación (en su versión móvil).

![used-tech](https://bytebucket.org/ng-plex/dts-project-assets/raw/4b8bf10df6e3a8c6e4a07298db9fc3874a319084/assets/mobile-app/used-tech.jpg?token=a51f0a09cfa0e3a83b83e1f4bba0038f1a26016c)

# Módulos

A nivel de Arquitectura, y aunque en este repositorio sólo se incluye el proyecto para dispositivos móviles, también está en desarrollo la aplicación de administración de incidencias que se encuentra en el siguiente repositorio [Aplicación de Administración](https://bitbucket.org/ng-plex/dts-manager).

Respecto a los módulos desarrollados, y/o en vías de desarrollo se incluyen:

- **Módulo de Autenticación y Autorización** basado en dos tipos de Autenticación que hacen uso de [Firebase](https://firebase.google.com/?hl=es-419) y [AngularFire2](https://github.com/angular/angularfire2):

    - Usuario y/o email, y contraseña 
    - Autenticación Federada basada en Redes Sociales

- **Módulo de Gestión de Incidencias** a través del cual el usuario podrá comunicar las incidencias a través de su dispositivo, pudiendo incorporar gran cantidad de información de forma sencilla:

    - Información descriptiva (título, descripción, tipología, etc.)

    - Información geográfica de la misma (dirección postal y coordenadas GPS) para facilitar su localización por los servicios operativos que se encarguen de la gestión de las mismas.

    - Información gráfica que permita identificar dicha incidencia

- **Módulo de Configuración** que permitirá al usuario incorporar una fotografía a su perfil, así como poder configurar la apariencia, así como otros parámetros como sería la opción de recibir notificaciones ante cualquier cambio en las incidencias remitidas, o el formato de fechas, entre otros. Esta sección está en proceso de actualización en estos momentos.

# Modelo de Datos

TO-DO ...

# Requisitos

Los siguientes requisitos son necesarios para un correcto funcionamiento de la aplicación:

- NodeJS v.8.9.0
- NPM v.5.5.1
- Angular v.5.0.3
- Ionic v.3.9.2
- Apache Cordova v.8.0.0

# Instalación

El proyecto puede usarse en su versión en desarrollo a través de un ambiente local mediante la clonación del proyecto y la configuración de **Firebase** tal y como se muestra a continuación.

```
$ git clone git@bitbucket.org:ng-plex/dts-user.git
```

Instalar las dependencias:

```
$ npm install
```

o si hace uso de [**Yarn**](https://yarnpkg.com/en/):

```
$ yarn install
```
Agregar los parámetros de configuración de Firebase en un archivo de tipo Environments `.env` en el siguiente **path**:

```
/src/env/environment.dev.ts
```

La inforamción se puede encontrar tras crear una aplicacíon a través de la consola de Firebase y el contenido básico es el siguiente:

```
import { FirebaseAppConfig } from 'angularfire2';
import * as firebase from 'firebase/app';

// Contenido agregado para configurar tanto la Base de Datos como el módulo de Autenticación y Autorización
export const environment = {
    production: false,
    firebase: {
        apiKey: "<API-KEY>",
        authDomain: "<AUTH-DOMAIN>.firebaseapp.com",
        databaseURL: "<DATABASE-URL>.firebaseio.com",
        projectId: "<PROJECT-ID>",
        storageBucket: "<STORAGE-BUCKET>.appspot.com",
        messagingSenderId: "<MESSAGING-SENDER-ID>"
    },
    gmaps_key: 'AIzaSyAjv3tGTyHuD4nsNRYEMislN783hUFnyN4'
};
```

> Reemplazar los valores entre **<>** por el contenido facilitado por la consola de Firebase.

Configurar los métodos de Autenticación (Email/Contraseña y Twitter, Google) y agregar la información al proyecto a través del archivo `config.xml`:

```
<universal-links>
        <host name="<APP-LINK>" scheme="https" />
        <host name="<FIREBASE-APP>.firebaseapp.com" scheme="https">
            <path url="/__/auth/callback" />
        </host>
</universal-links>
```

> NOTA. Para evitar un uso fraudulento de la aplicación base desarrollada por el autor de este repositorio se eliminará el proyecto alojado en Firebase lo que conlleva el cambio de credenciales así como cualquier otra información sensible incorporada en el proyecto actual, sin perjuicio del usuario o desarrollador que ejecute el proyecto en su entorno local.


Ejecutar el siguiente comando para probar la aplicación en el navegador:

```
$ ionic serve --lab
```

En caso de querer ejecutar el proyecto a través de un dispositivo móvil de tipo Android, deberemos hacer un build del proyecto y ejecutar los comandos que se detallan a continuación para generar el archivo `.apk` necesario para que se ejecute dicha aplicación en el dispositivo.

Para generar la aplicación y ejecutarla en el dispositivo deberemos conectar un terminal con Sistema Operativo Android a través del cable USB y ejecutar el siguiente comando:

```
$ ionic cordova run android
```

> Será necesario habilitar la depuración en el dispositivo para poder ejecutar dicha aplicación en el mismo.

# Secuencia de Uso de la Aplicación Móvil

Una vez instalada la aplicación móvil el usuario podrá proceder a remitir las incidencias que observe en la vía pública y crea necesario enviar para su evaluación y seguimiento por parte de la Entidad Gestora. Los pasos a ejecutar son los siguientes:

1. Registro o acceso mediante Autenticación a través de una cuenta de e-mail y/o una cuenta federada (Google o Twitter)

![authentication-screens](https://bytebucket.org/ng-plex/dts-project-assets/raw/530e2878bdbcdc7cfa79d7369296407bbd8272aa/assets/mobile-app/screenshots/auth-1x.png?token=8ebaaac4a4fcead19cdb7868b8533628a3e9eb59)

2. En caso de haber registrado anteriormente una cuenta podrá acceder al panel general en el cual, podrá navegar a través de un menú lateral a través de la aplicación. En principio será redigirido a un proceso de OnBoarding dónde se le explica en pequeños pasos cómo funciona dicha aplicación, y posteriormente podrá ver si la pantalla principal de incidencias (dónde se mostrarán en un futuro el listado de incidencias remitidas).

![listado](https://bytebucket.org/ng-plex/dts-project-assets/raw/6880891a6eb3a2d762fdaabc05a9e8130f43c1ec/assets/mobile-app/screenshots/screenshots-damages-list-3x.png?token=edad2ad766bf122d1ed480701516dee7bbcebff9)

3. Si detectara alguna incidencia y quiere comunicarla, podrá hacerlo mediante el botón habilitado para ello en la parte inferior derecha de la aplicación móvil. Se mostrará el siguiente formulario de Comunicación de Incidencias.

![comunicacion-incidencia](https://bytebucket.org/ng-plex/dts-project-assets/raw/530e2878bdbcdc7cfa79d7369296407bbd8272aa/assets/mobile-app/screenshots/new-damage-1x.png?token=3b0eb0ccdf5255842b66dbd325513e7eedfc04bf)

4. Una vez comunicada podrá consultarlas a través del listado principal, así como consultar los detalles de dicha incidencia.

![menu](https://bytebucket.org/ng-plex/dts-project-assets/raw/530e2878bdbcdc7cfa79d7369296407bbd8272aa/assets/mobile-app/screenshots/screenshot-9-sidenav-3x.png?token=fddb65f32e60df62ae69ce16aa79a0991a1e74a8)

Detalles de una incidencia:

![detalle](https://bytebucket.org/ng-plex/dts-project-assets/raw/6880891a6eb3a2d762fdaabc05a9e8130f43c1ec/assets/mobile-app/screenshots/details-1x.png?token=6a3fc53922c1a8c29a01b012b8f5815a080f9cdb)

5. Los parámetros de configuración así como el Perfil de Usuario podrá gestionarse mediante la pantalla de Configuración.

![profile](https://bytebucket.org/ng-plex/dts-project-assets/raw/530e2878bdbcdc7cfa79d7369296407bbd8272aa/assets/mobile-app/screenshots/profile-1x.png?token=106d94a3b4fc5bb871a7fe875ab8120877195972)

6. La ayuda está disponible a través del menú lateral.

7. Cuando vaya a dejar de usar la aplicación por un periodo prolongado de tiempo podrá salir de la misma para evitar un uso indebido o bien que la aplicación consuma recursos innecesarios.


# Changelog

## TODO:

:black_square_button: Actualizar el módulo de configuración.

:black_square_button: Agregar la pantalla de OnBoarding.

:negative_squared_cross_mark: Agregar los EMPTY-STATE para el listado de incidencias y para los mensajes y notificaciones.

:black_square_button: Mejoras en el Sistema de Comunicación de Notificaciones y Mensajería bidireccional (administrador <> Usuario).

:black_square_button: Incorporar la Autenticación federada vía Facebook y resolver los problemas de email compartido entre plataformas sociales.

:black_square_button: Mejoras en el proceso de subida de información multimedia sobre la incidencia (fotografía).

:black_square_button: Compartir a través de diversas plataformas o aplicaciones móviles. 

## [dev]

### Agregado
- Módulo base de autenticación y autorización mediante Firebaes Authentication.
- Módulo base de Gestión de Incidencias para el usuario general.
- Módulo base de Configuración de la aplicación móvil.


## [0.0.2] - 2018-01-16
### Agregado
- Migración del proyecto DTS a Bitbucket, y actualización e inicio del proyecto en Ionic 3+.

[dev]: https://bitbucket.org/ng-plex/dts-user/src/?at=develop
[0.0.2]: https://bitbucket.org/ng-plex/dts-user/src/?at=releases%2Fv0.0.2


# Licencia

The MIT License (MIT)

Copyright (c) 2018 Francisco J. Reyes (NGPlex.IO)

[Enlace a la Licencia](/LICENSE.md)

# Información relativa a la Privacidad de la información que almacene el usuario

> El usuario será responsable de la información remitida, así como de intentar no transmitir ninguna información multimedia (entiéndase cualquier fotografía en formato digital) que pueda incumplir la LOPD (Ley Orgánica de Protección de Datos) así como deberá evitar cualquier violación de la intimidad de los usuarios, Entidades Públicas o Privadas, de acuerdo a lo establecido en la legislación vigente.

# Referencias

[Angular5](https://angular.io/docs)

[Cordova](https://cordova.apache.org/)

[Ionic 3](ionicframework.com/docs)

[Firebase](https://firebase.google.com/?hl=es-419)

[AngularFire2](https://github.com/angular/angularfire2)

[Angular-maps](https://angular-maps.com/)
