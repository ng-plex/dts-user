import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DamagePage } from './damage';
import { CoreModule } from '../../app/core/core.module';
import { DamagesModule } from '../../app/damages/damages.module';

@NgModule({
  declarations: [
    DamagePage,
  ],
  imports: [
    CoreModule,
    DamagesModule,
    IonicPageModule.forChild(DamagePage),
  ],
})
export class DamagePageModule {}
