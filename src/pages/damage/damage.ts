import { AuthenticationService } from './../../app/core/authentication/shared/services/authentication/authentication.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { Observable } from 'rxjs/Observable';
import { DamagesService } from '../../app/damages/services/damages.service';

/**
 * Generated class for the DamagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import * as firebase from 'firebase/app';
import { DamageBase, DamageFull } from '../../app/damages/models';
import { User } from '../../app/core/authentication/shared/models/user.model';


@IonicPage()
@Component({
  selector: 'page-damage',
  templateUrl: 'damage.html',
})
export class DamagePage {
  damage: DamageFull;
  authenticatedUser: User;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _damagesServ: DamagesService,
    private _authServ: AuthenticationService
  ) {
  }

  ngOnInit() {
    this.damage = this.navParams.get('damage');
    this.authenticatedUser = firebase.auth().currentUser;
    // this._damagesServ.getDamageByKey(damageKey).subscribe(damage => {
    //   this.damage$ = this._damagesServ.getFullDamageObject(damage);
    // });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DamagePage');
  }

  ionViewCanEnter() {
    return firebase.auth().currentUser;
  }

}
