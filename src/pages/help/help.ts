import { AuthenticationService } from './../../app/core/authentication/shared/services/authentication/authentication.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import * as firebase from 'firebase/app';


@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _authServ: AuthenticationService
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }

  ionViewCanEnter() {
    return firebase.auth().currentUser;
  }

}
