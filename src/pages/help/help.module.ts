import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpPage } from './help';

// Core Module
import { CoreModule } from './../../app/core/core.module';


@NgModule({
  declarations: [
    HelpPage,
  ],
  imports: [
    CoreModule,
    IonicPageModule.forChild(HelpPage),
  ],
})
export class HelpPageModule {}
