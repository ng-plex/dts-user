import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthenticationService } from '../../app/core/authentication/shared/services/authentication/authentication.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  auth$ = this._authServ.authUser$;
  constructor(public navCtrl: NavController, private _authServ: AuthenticationService) {

  }

}
