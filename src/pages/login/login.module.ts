import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';


// Core Module
import { CoreModule } from './../../app/core/core.module';


@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    CoreModule,
    IonicPageModule.forChild(LoginPage),
  ],
})
export class LoginPageModule {}
