import { AuthenticationService } from './../../app/core/authentication/shared/services/authentication/authentication.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import 'rxjs/add/operator/switchMap';

// firebase and angularfire2
import * as firebase from 'firebase/app';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { UserSettings, SettingsService } from '../../app/profile/services/settings/settings.service';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  selectedTab: string = "profile";
  user$ = this._authServ.authUser$;
  settings$: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _authServ: AuthenticationService,
    private _settingsServ: SettingsService,
    private _afDB: AngularFireDatabase) {
  }

  ionViewCanEnter() {
    return firebase.auth().currentUser;
  }

  ionViewDidLoad() {
    this._authServ.authUser$.subscribe((user) => {
      this.getSettings(user).valueChanges().subscribe((result) => this.settings$ = result);
    })
  }

  getSettings(user) {
    let userID = user.uid;
    return this._afDB.object(`settings/${userID}`);
  }

  subirFoto() { }

}
