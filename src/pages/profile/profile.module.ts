import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';

// Core Module
import { CoreModule } from '../../app/core/core.module';
import { ProfileModule } from '../../app/profile/profile.module';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    CoreModule,
    ProfileModule,
    IonicPageModule.forChild(ProfilePage),
  ],
  exports: [
  ]
})
export class ProfilePageModule {}
