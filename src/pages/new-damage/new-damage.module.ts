import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewDamagePage } from './new-damage';

// Core Module
import { CoreModule } from './../../app/core/core.module';
import { DamagesModule } from '../../app/damages/damages.module';


@NgModule({
  declarations: [
    NewDamagePage,
  ],
  imports: [
    CoreModule,
    DamagesModule,
    IonicPageModule.forChild(NewDamagePage),
  ],
})
export class NewDamagePageModule {}
