import { DamagesService } from './../../app/damages/services/damages.service';
import { DamageBase } from './../../app/damages/models/damage.model';
import { LoadingService } from './../../app/core/shared/services/loading.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from '@firebase/util';

import { AuthenticationService } from './../../app/core/authentication/shared/services/authentication/authentication.service';
/**
 * Generated class for the NewDamagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import * as firebase from 'firebase/app';


@IonicPage()
@Component({
  selector: 'page-new-damage',
  templateUrl: 'new-damage.html',
})
export class NewDamagePage {
  currentUser$: any;
  newDamage: DamageBase;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingServ: LoadingService,
    public _authServ: AuthenticationService,
    public _damageServ: DamagesService
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewDamagePage');
    this.currentUser$ = this._authServ.currentUser$;
  }

  ionViewCanEnter() {
    return firebase.auth().currentUser;
  }

  onSaveNewDamage(_newDamage: DamageBase) {
    let newDamage = _newDamage;
    this._damageServ.insertDamage(newDamage);
    this.navCtrl.pop();
  }

}
