import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationsPage } from './notifications';

// Core Module
import { CoreModule } from './../../app/core/core.module';

@NgModule({
  declarations: [
    NotificationsPage,
  ],
  imports: [
    CoreModule,
    IonicPageModule.forChild(NotificationsPage),
  ],
})
export class NotificationsPageModule {}
