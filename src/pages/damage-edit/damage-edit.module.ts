import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DamageEditPage } from './damage-edit';

// Core Module
import { CoreModule } from './../../app/core/core.module';
import { DamagesModule } from '../../app/damages/damages.module';


@NgModule({
  declarations: [
    DamageEditPage,
  ],
  imports: [
    CoreModule,
    DamagesModule,
    IonicPageModule.forChild(DamageEditPage),
  ],
})
export class DamageEditPageModule {}
