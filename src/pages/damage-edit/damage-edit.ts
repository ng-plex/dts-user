import { AuthenticationService } from './../../app/core/authentication/shared/services/authentication/authentication.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import * as firebase from 'firebase/app';


/**
 * Generated class for the DamageEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-damage-edit',
  templateUrl: 'damage-edit.html',
})
export class DamageEditPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _authServ: AuthenticationService
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DamageEditPage');
  }

  ionViewCanEnter() {
    return firebase.auth().currentUser;
  }

}
