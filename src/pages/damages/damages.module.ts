import { DamagesModule } from './../../app/damages/damages.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

// Damages Module
import { DamagesPage } from './damages';

// Core Module
import { CoreModule } from './../../app/core/core.module';

@NgModule({
  declarations: [
    DamagesPage,
  ],
  imports: [
    CoreModule,
    DamagesModule,
    IonicPageModule.forChild(DamagesPage),
  ],
})
export class DamagesPageModule {}
