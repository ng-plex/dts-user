import { AuthenticationService } from './../../app/core/authentication/shared/services/authentication/authentication.service';
import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DamagesService } from '../../app/damages/services/damages.service';
import { Observable } from 'rxjs/Observable';
import { DamageBase } from './../../app/damages/models';

import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-damages',
  templateUrl: 'damages.html',
})
export class DamagesPage {
  damages$: Observable<any>;
  user$ = this._authServ.authUser$;
  changeNoDamageBlankState: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _damagesServ: DamagesService,
    private _authServ: AuthenticationService,
  ) { }

  ionViewCanEnter() {
    return firebase.auth().currentUser;
  }
  
  ngOnInit() {
    //Get the list reverse based on the created_at field
    this.user$.subscribe(user => {
      this.damages$ = this._damagesServ.getAllUserDamages(user.uid).map((damages) => damages.reverse());
    });
  }

  viewDamage(key: string) {
    this.navCtrl.push('DamagePage', { key: key });
  }

  goToNewDamage() {
    this.navCtrl.push('NewDamagePage');
  }
}
