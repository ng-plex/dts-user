import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';

// Core Module
import { CoreModule } from '../../app/core/core.module';

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    CoreModule,
    IonicPageModule.forChild(RegisterPage),
  ],
})
export class RegisterPageModule {}
