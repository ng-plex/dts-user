import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthenticationService } from './../../app/core/authentication/shared/services/authentication/authentication.service';
import { User } from '../../app/core/authentication/shared/models/user.model';

import * as firebase from 'firebase/app';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  currentUser$: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _authServ: AuthenticationService
  ) {
  }

  ionViewDidLoad() {
    this.currentUser$ = this._authServ.currentUser$;
  }

  ionViewCanEnter() {
    return !firebase.auth().currentUser;
  }

}
