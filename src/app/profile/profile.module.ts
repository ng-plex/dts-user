import { DamagesModule } from './../damages/damages.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Ionic Module
import { IonicModule } from 'ionic-angular/module';

// Custom Components
import { ProfileBasicComponent } from './components/profile-basic/profile-basic.component';
import { ProfileEditorComponent } from './components/profile-editor/profile-editor.component';
import { SettingsEditorComponent } from './components/settings-editor/settings-editor.component';

// Third-party Plugins
import { Camera } from '@ionic-native/camera';

// Custom Services
import { ProfileService } from './services/profile/profile.service';
import { SettingsService } from './services/settings/settings.service';

import { MomentPipe } from './../damages/pipes/moment.pipe';

@NgModule({
    declarations: [
        ProfileBasicComponent,
        ProfileEditorComponent,
        SettingsEditorComponent,
    ],
    imports: [
        CommonModule,
        DamagesModule,
        IonicModule,
    ],
    exports: [
        ProfileBasicComponent,
        ProfileEditorComponent,
        SettingsEditorComponent,
    ],
    providers: [
        Camera,
        ProfileService,
        SettingsService,
    ]
})
export class ProfileModule { }
