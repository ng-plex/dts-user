import { AuthenticationService } from './../../../core/authentication/shared/services/authentication/authentication.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import { FormBuilder, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

import * as firebase from 'firebase/app';
import { SettingsService, UserSettings } from '../../services/settings/settings.service';

import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
    selector: 'settings-editor',
    templateUrl: 'settings-editor.component.html'
})
export class SettingsEditorComponent implements OnInit {
    @Input() user: firebase.User;
    @Input() settings: UserSettings;
    public settingsForm: FormGroup;

    // Theme UIs Array values
    public themeUIs = [
        { val: 0, text: 'Defecto' },
        { val: 1, text: 'Secundario' },
    ];

    // Date Formats
    public dateFormats = [
        { val: 0, format: 'DIA-MES-AÑO' },
        { val: 1, format: 'MES-DIA-AÑO' },
        { val: 2, format: 'DIA/MES/AÑO' },
        { val: 3, format: 'MES/DIA/AÑO' },
        { val: 4, format: 'AMIGABLE (Ej. hace 2 días)' }
    ]

    public selectedPrimaryColor: string;
    public selectedSecondaryColor: string;

    constructor(
        private _fb: FormBuilder,
        private _settingsServ: SettingsService,
        private _authServ: AuthenticationService,
    ) {

    }

    ngOnChanges(changes: SimpleChanges) {
        this.settingsForm = this._fb.group({
            selectedTheme: [this.settings.themeUI, Validators.required],
            dateFormat: [this.settings.dateFormat, Validators.required],
            beNotified: [this.settings.beNotified, Validators.required],
        });
    }

    ngOnInit() {
    }

    onSubmit() {
        if (this.settingsForm.valid) {
            this._settingsServ.userSettings = {
                beNotified: this.settingsForm.get('beNotified').value,
                themeUI: this.settingsForm.get('selectedTheme').value,
                dateFormat: this.settingsForm.get('dateFormat').value
            };
            this._settingsServ.save(this.user);
        };


    }

}