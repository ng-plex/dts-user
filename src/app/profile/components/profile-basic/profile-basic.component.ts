import { Component, OnInit, Input } from '@angular/core';

// Profile Service
import { ProfileService } from '../../services/profile/profile.service';

@Component({
    selector: 'profile-basic',
    templateUrl: 'profile-basic.component.html'
})
export class ProfileBasicComponent implements OnInit {

    @Input() user: any;

    constructor(private _profServ: ProfileService) { }

    ngOnInit() { }

    subirFoto() {
        this._profServ.uploadPhoto();
    }

}