import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

// Authentication Service
import * as firebase from 'firebase/app';
import { AuthenticationService } from './../../../core/authentication/shared/services/authentication/authentication.service';

@Component({
    selector: 'profile-editor',
    templateUrl: 'profile-editor.component.html'
})

export class ProfileEditorComponent implements OnInit {
    @Input() user: firebase.User;

    profEditForm: FormGroup;

    constructor(
        private _fb: FormBuilder,
        private _authSrv: AuthenticationService
    ) { }

    ngOnInit() {
        this.profEditForm = this._fb.group({
            newUsername: ['', Validators.compose([Validators.required, Validators.minLength(3)])]
        });
    }

    onSubmit() {
        if (!this.profEditForm.valid) {
            return
        }

        this._authSrv.updateProfile(this.user, this.profEditForm.get('newUsername').value, this.user.photoURL);
    }

    get newUserNameValid() {
        const control = this.profEditForm.get('newUsername');
        return control.hasError('required') || control.hasError('minLength')
            && control.touched;
    }
}