import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

// Authentication Service
import { AuthenticationService } from './../../../core/authentication/shared/services/authentication/authentication.service';

// Firebase and AngularFire2
import * as firebase from 'firebase/app';
import 'firebase/storage';

import { AngularFireDatabase } from 'angularfire2/database';

// Third-party Plugins
// Camera Plugin
import { Camera, CameraOptions } from '@ionic-native/camera';

@Injectable()
export class ProfileService {
    public base64Img: any;
    public error: any;
    public textoError: string;
    public loadProgress: string;
    public subidaCompletada: boolean = false;
    public subiendo: boolean = false;

    constructor(
        private _authServ: AuthenticationService,
        private _afDB: AngularFireDatabase,
        private _camera: Camera,
        private _toastCtrl: ToastController
    ) { }

    // TODO: Update the display name
    updateDisplayName() {

    }

    uploadPhoto() {
        const options: CameraOptions = {
            quality: 70,
            destinationType: this._camera.DestinationType.DATA_URL,
            encodingType: this._camera.EncodingType.JPEG,
            mediaType: this._camera.MediaType.PICTURE,
            sourceType: this._camera.PictureSourceType.PHOTOLIBRARY
        }

        let storageRef = firebase.storage().ref();
        let userID: string = firebase.auth().currentUser.uid;
        let timestamp: number = (Date.now() / 1000);
        let imgFileName: string = `${timestamp}_${userID}`;
        let storageFilePath = storageRef.child(`photos/${imgFileName}`)
        this._camera.getPicture(options).then((imageData) => {
            let dataImg = 'data:image/jpeg;base64,' + imageData;
            let photo = this.dataURItoBlob(dataImg);
            let subidaTask = storageFilePath.put(photo);
            subidaTask.on('state_changed', (snapshot: firebase.storage.UploadTaskSnapshot) => {
                this.subiendo = true;
                this.loadProgress = ((snapshot.bytesTransferred / snapshot.totalBytes) * 100).toFixed(2);
            }, (error) => {

            }, () => {
                this.subidaCompletada = true;
                this.subiendo = false;
                let toastSuccesImg = this._toastCtrl.create({
                    message: 'Imagen subida con éxito',
                    duration: 3000
                }).present();
                firebase.auth().currentUser.updateProfile({
                    displayName: firebase.auth().currentUser.displayName,
                    photoURL: subidaTask.snapshot.downloadURL
                });
            });
        });
    }

    dataURItoBlob(dataURI) {
        // code adapted from: http://stackoverflow.com/questions/33486352/cant-upload-image-to-aws-s3-from-ionic-camera
        let binary = atob(dataURI.split(',')[1]);
        let array = [];
        for (let i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
    };
}