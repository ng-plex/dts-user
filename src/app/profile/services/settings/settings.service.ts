import { Injectable } from '@angular/core';

// Firebase Imports
import * as firebase from 'firebase/app';
import { AngularFireDatabase, snapshotChanges } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

export interface UserSettings {
  uid?: string;
  themeUI: string;
  dateFormat: string;
  beNotified: boolean;
}

export const DEFAULT_USER_SETTINGS: UserSettings = {
  uid: null,
  themeUI: '0',
  dateFormat: '0',
  beNotified: false
};

@Injectable()
export class SettingsService {
  // Settings Demo Object
  private _userSettings: UserSettings = DEFAULT_USER_SETTINGS;

  constructor(
    private _afDB: AngularFireDatabase,
    private _afAuth: AngularFireAuth
  ) { }

  get userSettings() {
    return this._userSettings;
  }

  set userSettings(settings: UserSettings) {
    this._userSettings =  Object.assign({}, this._userSettings, settings);
  }

  save(user: firebase.User) {
    return new Promise((resolve, reject) => {
      if (!user) {
        reject(new Error('No se pueden almacenar los ajustes ya que el usuario no se encuentra en nuestra Base de Datos'));
      } else {
        let ref = this._afDB.object(`settings/${user.uid}`);
        resolve(ref.set(this._userSettings));
      }
    });
  }

  restore(user: firebase.User) {
    return new Promise((resolve, reject) => {
      if (!user) {
        reject(new Error('No se pueden restaurar los ajustes del Usuario, ya que no se encuentra en nuestra Base de Datos'));
      } else {
        let ref = this._afDB.object(`settings/${user.uid}`);
        resolve(ref.set(DEFAULT_USER_SETTINGS));
      }
    });
  }

  getSettings(user: firebase.User) {
    let ref = this._afDB.object(`settings/${user.uid}`);
    return new Promise((resolve, reject) => {
      if (!user) {
        reject(new Error('No se pueden obtener los ajustes del Usuario, ya que no se encuentra en nuestra Base de Datos'))
      } else {
        ref.snapshotChanges().subscribe((action) => {
          let data = action.payload;
          resolve(data.val());
        })
      }
    });
  }


}
