import { User } from "../../core/authentication/shared/models/user.model";

export interface Address {
    line1?: string;
    city?: string;
    country?: string;
    zipcode?: string;
}

export interface Coords {
    lat?: number;
    lng?: number;
}

export interface DamageBase {
    $key?: string;
    title?: string;
    type?: string;  //Based on the DamageStatus Key
    description?: string;
    address?: Address; //Complete Address Object
    coords?: Coords; //Complete Coords Object
    userID?: string;
    photoURL?: string;
    status?: string; //Based on the DamageType Key
    created_at?: number;
    updated_at?: number;
}

export interface DamageFull {
    $key?: string;
    title?: string;
    description?: string;
    photoURL?: string;
    address?: Address; //Complete Address Object
    coords?: Coords; //Complete Coords Object
    user?: User;
    type?: string; //Based on a query of the DamageType
    status?: string; //Based on a query of the DamageStatus
    created_at?: number;
    updated_at?: number;
}

export interface DamageType {
    $key?: string;
    value: string;
}

export interface DamageStatus {
    $key?: string;
    value: string;
}
