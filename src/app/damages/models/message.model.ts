import { DamageBase } from './damage.model';
import { User } from './../../core/authentication/shared/models/user.model';

// Base Damage Message
export interface Message {
    $key?: string;
    senderUID?: string;
    title: string;
    text: string;
    sent_at: number;
    read?: boolean;
}