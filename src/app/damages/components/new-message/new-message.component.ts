import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { DamageBase } from '../../models';

@Component({
    selector: 'new-message',
    templateUrl: 'new-message.component.html'
})
export class NewMessageComponent implements OnInit {
    @Input() damage: DamageBase;

    public newMessageForm: FormGroup = this._fb.group({
        title: ['', Validators.required],
        text: ['', Validators.required],
    });


    constructor(
        private _fb: FormBuilder
    ) {

    }

    ngOnInit() {

    }

    onSubmit() {

    }

    isTitleValid() {
        return this.newMessageForm.get('title').valid && this.newMessageForm.get('title').touched;
    }
    
    isTextValid() { 
        return this.newMessageForm.get('text').valid && this.newMessageForm.get('text').touched;

    }
}