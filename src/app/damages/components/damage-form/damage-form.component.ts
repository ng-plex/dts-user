import { Component, OnInit, Input, EventEmitter, Output, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

// Services
import { DamagesService } from '../../services/damages.service';

// Models
import { DamageBase } from '../../models';

// Observables
import { Observable } from 'rxjs/Observable';

// Geolocation capabilities
import { Platform, Checkbox } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

// Moment
import * as moment from 'moment';

@Component({
    selector: 'damage-form',
    templateUrl: 'damage-form.component.html'
})
export class DamageFormComponent implements OnInit {

    //The form is ready for submit
    formReadyToSubmit: boolean = false;

    public newDamage: DamageBase;
    //Current User for getting the User ID.
    @Input() currentUser: any;
    @Output() dataSaved: EventEmitter<DamageBase> = new EventEmitter();

    photoURL: Observable<string>;


    // Boolean field to add or not coords to the DamageBase item (evaluate after if exists for show map or not)

    addCoords: boolean = false;
    position: any;

    selectedCity: string = 'MLG';
    selectedCountry: string = 'ES';
    fotoDisponible: boolean = false;
    // Issue for the ion-datetime default Date format with TimeZones
    // https://github.com/ionic-team/ionic/issues/11408#issuecomment-352167466
    // Alternative withouth the local TimeZone 
    // defaultDate = new Date().toISOString();
    defaultDate = moment().format();

    //New Damage Form FormGroup
    public newDamageForm: FormGroup = this._fb.group({
        title: ['', Validators.required],
        description: ['', Validators.required],
        type: ['-1', Validators.required],
        address: this._fb.group({
            line1: ['', Validators.required],
            city: ['MLG', Validators.required],
            country: ['ES', Validators.required],
            zipcode: ['', Validators.required]
        }),
        coords: this._fb.group({
            lat: [''],
            lng: ['']
        }),
        created_at: [this.defaultDate, Validators.required]
    });

    damageTypes$: Observable<any>;
    damageStatuses$: Observable<any>;


    // Helper arrays
    monthNames = "enero, febrero, marzo, abril, mayo, junio, julio, agosto, septiembre, octubre ,noviembre, diciembre"
    monthsShortnames = "ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic";
    days = "domingo, lunes, martes, miércoles, jueves, viernes, sábado";
    shortDays = "dom, lun, mar, mie, jue, vie, sab";


    cities = [
        { val: 'MLG', text: 'Málaga' },
        { val: 'MAD', text: 'Madrid' },
        { val: 'BCN', text: 'Barcelona' },
        { val: 'SEV', text: 'Sevilla' }
    ];

    countries = [
        { val: 'ES', text: 'España' }
    ]


    constructor(
        private _damageServ: DamagesService,
        private _fb: FormBuilder,
        private geolocation: Geolocation,
        private platform: Platform
    ) {

    }


    ngOnInit() {
        this.damageStatuses$ = this._damageServ.getDamageStatusesList();
        this.damageTypes$ = this._damageServ.getDamageTypesList();
    }


    /**
     * Set the PhotoURL from the upload component into our PhotoURL string property
     */

    setPhotoURL(_photoURL: Observable<string>) {
        this.photoURL = _photoURL;
        this.photoURL.subscribe((saved) => {
            this.formReadyToSubmit = true;
        });
    }

    /**
     * Return a number relative to the milliseconds of a given ISO Date
     * @param isoDate string
     */
    setTimestampFromISODate(isoDate: string): number {
        return (new Date(isoDate).getTime() / 1000);
    }

    /**
     * Emit the event with the submit form
     */

    onSubmit() {
        // Emit when the photoURL was loaded
        this.photoURL.subscribe((photoURL) => {
            let damageValue: DamageBase = this.newDamageForm.value;
            let city = this.objectKeyToValue(this.newDamageForm.value.address.city, this.cities);
            let country = this.objectKeyToValue(this.newDamageForm.value.address.country, this.countries);
            this.newDamage = {
                ...this.newDamageForm.value,
                status: 'status_001',
                address: {
                    ...damageValue.address,
                    city: city,
                    country: country,
                    zipcode: Number(damageValue.address.zipcode)
                },
                created_at: this.setTimestampFromISODate(this.newDamageForm.value['created_at']),
                updated_at: this.setTimestampFromISODate(this.newDamageForm.value['created_at']),
                userID: this.currentUser.uid,
                photoURL: photoURL
            }
            this.dataSaved.emit(this.newDamage);
        });
    }

    /**
     * Extract the TEXT value from a queried Object inside an Array
     * 
     * @param key string
     * @param _collection Array
     */
    private objectKeyToValue(key: string, _collection: Array<any>): string {
        let obj: { val: string, text: string } = _collection.find(o => o.val === key);
        return obj.text;
    }


    // Helper methods
    changeAddCoords(check) {
        this.addCoords = check.value;
        if (this.addCoords) {
            // get position
            this.geolocation.getCurrentPosition({ enableHighAccuracy: true, timeout: 10000, maximumAge: 0 })
                .then((pos) => {
                    this.position = pos;
                    this.newDamageForm.get('coords.lat').setValue(pos.coords.latitude);
                    this.newDamageForm.get('coords.lng').setValue(pos.coords.longitude);
                }, (err) => {
                    console.error(err);
                });

            let watcher = this.geolocation.watchPosition()
                .subscribe((pos) => {
                    this.position = pos;
                    this.newDamageForm.get('coords.lat').setValue(pos.coords.latitude);
                    this.newDamageForm.get('coords.lng').setValue(pos.coords.longitude);
                }, (err) => console.error(err));

            watcher.unsubscribe();
        } else {
            this.newDamageForm.get('coords.lat').reset('');
            this.newDamageForm.get('coords.lng').reset('');
        }
    }
}