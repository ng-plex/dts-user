import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

// Models
import { Message } from './../../models/message.model';
import { User } from '../../../core/authentication/shared/models/user.model';

// Services
import * as firebase from 'firebase/app';
import { DamagesService } from './../../services/damages.service';
import { MessagesService } from './../../services/messages.service';

@Component({
    selector: 'message-item',
    templateUrl: 'message-item.component.html'
})
export class MessageItemComponent implements OnInit {
    @Input() authenticatedUser: firebase.User;
    @Input() message: Message

    public selectedMessage: Message;

    @Output() markedRead: EventEmitter<Message> = new EventEmitter<Message>();

    ngOnInit() {}

    //  Method for mark as read one Damage Message
    markAsRead() {
        this.markedRead.emit(this.selectedMessage);
    }
}