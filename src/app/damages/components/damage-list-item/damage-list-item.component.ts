import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import * as moment from 'moment';
import { NavController } from 'ionic-angular';

import { DamageBase } from './../../models';
import { DamagesService } from './../../services/damages.service';

@Component({
    selector: 'damage-list-item',
    templateUrl: 'damage-list-item.component.html'
})
export class DamageListItemComponent implements OnInit {
    

    @Input() damage: DamageBase;

    damageStatus: string;

   

    // @Output() viewDamage = new EventEmitter<any>();
    constructor(
        private _navCtrl: NavController,
        private _damageServ: DamagesService
    ) { }

    ngOnInit() {
        
    }

    openDamage(damage: any) {
        this._navCtrl.push('DamagePage', { damage: damage })
    }

}