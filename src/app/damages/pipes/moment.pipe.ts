import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
    name: 'moment'
})
export class MomentPipe implements PipeTransform {

    transform(value: number, format: string) {
        moment.locale('es');
        return moment(moment.unix(value), format).fromNow();
    }

}