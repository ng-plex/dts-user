import { User } from './../../../core/authentication/shared/models/user.model';
import { DamageFull } from './../../models/damage.model';
import { Observable } from 'rxjs/Observable';
import { Platform, NavParams } from 'ionic-angular';
import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../models';

// Services
import * as firebase from 'firebase/app';
import { MessagesService } from './../../services/messages.service';


@Component({
    selector: 'damage-detail',
    templateUrl: 'damage-detail.component.html'
})
export class DamageDetailComponent {
    @Input() damage: DamageFull;
    @Input() authenticatedUser: firebase.User;
    public messages$: Observable<Message[]>;

    selectedDamageTab: string = 'description';

    constructor(
        private _messageServ: MessagesService
    ) {

    }

    ngOnInit() {
        this.messages$ = this._messageServ.getAllMessagesByDamageKey(this.damage.$key)
            .map((messages) => messages.reverse());
    }

}