import { DamagesService } from './../../services/damages.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'damages-list',
    templateUrl: 'damages-list.component.html'
})
export class DamagesListComponent implements OnInit {
    @Input() damages: any;
    @Output() viewDamage = new EventEmitter<any>();

    damagesByUser$: any;


    constructor(private damagesServ: DamagesService) { }

    ngOnInit() {
    }

    onOpenDamage(key: string) {
        this.viewDamage.emit(key);
    }

    

}