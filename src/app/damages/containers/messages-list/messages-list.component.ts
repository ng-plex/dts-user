import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

import { Observable } from 'rxjs/Observable';

// Services
import * as firebase from 'firebase/app';
import { DamagesService } from './../../services/damages.service';
import { MessagesService } from './../../services/messages.service';


// Models
import { User } from '../../../core/authentication/shared/models/user.model';
import { Message, DamageFull, DamageBase } from '../../models';

@Component({
    selector: 'messages-list',
    templateUrl: 'messages-list.component.html'
})
export class MessagesListComponent {
    @Input() authenticatedUser: firebase.User;
    @Input() damage: DamageFull;
    @Input() messages: Observable<Message[]>;

    constructor(
        private _damageServ: DamagesService,
        private _messagesServ: MessagesService
    ) { }


    onMessageRead(message: Message) {
        this._messagesServ.markAsRead(message.$key);
    }
}