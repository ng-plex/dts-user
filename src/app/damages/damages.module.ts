import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Ionic Module
import { IonicModule } from 'ionic-angular/module';
import { SharedModule } from './../core/shared/shared.module';

// Custom Containers
import { DamagesListComponent } from './containers/damages-list/damages-list.component';
import { DamageDetailComponent } from './containers/damage-detail/damage-detail.component';
import { MessagesListComponent } from './containers/messages-list/messages-list.component';

// Custom Components
import { DamageListItemComponent } from './components/damage-list-item/damage-list-item.component';
import { DamageFormComponent } from './components/damage-form/damage-form.component';
import { MessageItemComponent } from './components/message-item/message-item.component';
import { NewMessageComponent } from './components/new-message/new-message.component';

// Third-party Plugins
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';

// Custom Services
import { DamagesService } from './services/damages.service';
import { MessagesService } from './services/messages.service';
import { MomentPipe } from './pipes/moment.pipe';

import { AgmCoreModule } from '@agm/core';
import { environment } from '../../env/environment.dev';

// AngularFire2 Storage Module
import { AngularFireStorageModule } from 'angularfire2/storage';

@NgModule({
    declarations: [
        DamagesListComponent,
        DamageListItemComponent,
        DamageDetailComponent,
        DamageFormComponent,
        MessagesListComponent,
        MessageItemComponent,
        NewMessageComponent,
        MomentPipe,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IonicModule,
        AngularFireStorageModule,
        AgmCoreModule.forRoot({
            apiKey: environment.gmaps_key
        }),
        SharedModule,
    ],
    exports: [
        DamagesListComponent,
        DamageListItemComponent,
        DamageDetailComponent,
        DamageFormComponent,
        MomentPipe,
    ],
    providers: [
        Camera,
        DamagesService,
        MessagesService,
        Geolocation,
    ]
})
export class DamagesModule { }
