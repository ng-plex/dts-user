import { Injectable } from '@angular/core';

// Observables
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap'; //Incluyes flatMap operator
import 'rxjs/add/observable/combineLatest';

// AngularFire2 and firebase
import * as firebase from 'firebase/app';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { User } from './../../core/authentication/shared/models/user.model';
import { Message } from '../models';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class MessagesService {

    constructor(
        private _afDB: AngularFireDatabase
    ) { }

    getAllMessagesByDamageKey(damageKey: string) {
        return this._afDB.list(`damages_messages/${damageKey}/messages`, ref => {
            return ref.orderByChild('sent_at')
        }).snapshotChanges()
            .map((changes) => changes.map((change) => {
                const $key = change.key;
                const data: Message = {
                    $key: $key,
                    ...change.payload.val(),
                };
                return data;
            }));
    }

    // Service method for mark as read one Damage Message
    // TODO: Reorganize the RTDB for query based on the DamageKey not the MessageKey in
    // order to get only the Damage Messages (Security Rules)
    markAsRead(messageKey: string) {
        this._afDB.object(`damage_messages/${messageKey}`).update({
            read: true
        })
    }
}