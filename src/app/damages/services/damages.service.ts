import { Injectable } from "@angular/core";

// Observables and RxJS library imports
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap'; //Incluyes flatMap operator
import 'rxjs/add/observable/combineLatest';

// AngularFire imports
import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject
} from 'angularfire2/database';

// Firebase and AngularFire2
import * as firebase from 'firebase/app';
import 'firebase/storage';

import { ToastController } from 'ionic-angular';


// Authentication Service
import { AuthenticationService } from './../../core/authentication/shared/services/authentication/authentication.service';

// Third-party Plugins
// Camera Plugin
import { Camera, CameraOptions } from '@ionic-native/camera';

// User Elements
import { User } from '../../core/authentication/shared/models/user.model';

// Damage Elements
import { DamageBase, DamageFull } from '../models';

@Injectable()
export class DamagesService {
  // Camera properties
  public base64Img: any;
  public error: any;
  public textoError: string;
  public loadProgress: string;
  public subidaCompletada: boolean = false;
  public subiendo: boolean = false;

  constructor(
    private _afDb: AngularFireDatabase,
    private _authServ: AuthenticationService,
    private _afDB: AngularFireDatabase,
    private _camera: Camera,
    private _toastCtrl: ToastController
  ) {

  }

  /**
   * Helper method for getting the User based on the UID stored in the Damage uid property
   * @param uid string
   * @returns Observable<User>
   */
  getAuthUserBasedOnDamage(uid: string): Observable<User> {
    return this._afDb.object<User>(`/users/${uid}`).valueChanges();
  }

  /**
   * Extract all Damages without take into account the Authenticated User
   */
  getAllDamages(): Observable<DamageBase[]> {
    return this._afDb.list<DamageBase>('/damages').valueChanges();
  }

  /**
   * Extract all Authenticated User Damages 
   */
  getAllUserDamages(uid: string) {
    const keys$ = this._afDb.list(`user_damages/${uid}`, (ref) => {
      return ref.orderByChild('created_at')
    }).snapshotChanges()
      .map((actions) => {
        return actions.map((a) => {
          return a.key;
        })
      });

    return keys$
      .map((damageKeys) => damageKeys
        .map((damageKey) => {
          return this._afDb.object(`damages/${damageKey}`).snapshotChanges().map((change) => {
            return { $key: change.key, ...change.payload.val() }
          }).switchMap((item) => this.getFullDamageObject(item))
        }))
      .flatMap((res) => {
        return Observable.combineLatest(res);
      }
      )
  }

  /**
   * Get a DamageBase based on its Key
   * @param damageKey string
   * @returns Observable<DamageBase>
   */
  getDamageByKey(damageKey: string) {
    return this._afDb.object<DamageBase>(`damages/${damageKey}`).valueChanges();
  }

  /**
   * Get a Full Damage Object based on a DamageBase one
   */
  getFullDamageObject(damage: DamageBase): Observable<DamageFull> {
    let damageType;
    let damageStatus;
    this.getDamageTypeByKey(damage.type).subscribe(type => damageType = type);
    this.getDamageStatusByKey(damage.status).subscribe(status => damageStatus = status);
    return this.getAuthUserBasedOnDamage(damage.userID)
      .map((user: User) => {
        return {
          $key: damage.$key,
          title: damage.title,
          description: damage.description,
          address: damage.address,
          coords: damage.coords,
          photoURL: damage.photoURL,
          user: user,
          type: damageType, //(<string>damageType).toUpperCase(),
          status: damageStatus,
          created_at: damage.created_at,
          updated_at: damage.updated_at
        };
      });
  }

  /**
   * Get a full Damage Types list
   */
  getDamageTypesList() {
    return this._afDb.list(`damage_types`).snapshotChanges()
      .map((changes) => changes.map(c => {
        return { $key: c.key, value: c.payload.val() };
      }));

  }

  /**
   * Get a Damage Type based on its type Key
   * @param typeKey string
   * @returns Observable<any>
   */
  getDamageTypeByKey(typeKey: string): Observable<any> {
    return this._afDb.object<any>(`damage_types/${typeKey}`).valueChanges();
  }

  /**
   * Get a full Damage Statuses list
   */
  getDamageStatusesList() {
    return this._afDb.list(`damage_statuses`).valueChanges();
  }

  /**
   * Get a Damage Status based on its status Key
   * @param statusKey string
   * @returns Observable<any>
   */
  getDamageStatusByKey(statusKey: string) {
    return this._afDb.object<any>(`damage_statuses/${statusKey}`).valueChanges();
  }

  /**
 * Upload the Photograph
 * TODO: Make separated methods get the picture and send to the server (Firebase Storage and Firebase Real Time Database)
 */
  uploadPhoto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this._camera.DestinationType.DATA_URL,
      encodingType: this._camera.EncodingType.JPEG,
      mediaType: this._camera.MediaType.PICTURE,
      sourceType: this._camera.PictureSourceType.PHOTOLIBRARY
    }

    let storageRef = firebase.storage().ref();
    let userID: string = firebase.auth().currentUser.uid;
    let timestamp: number = (Date.now() / 1000);
    let imgFileName: string = `${timestamp}_${userID}`;
    let storageFilePath = storageRef.child(`photos/${imgFileName}`)
    this._camera.getPicture(options).then((imageData) => {
      let dataImg = 'data:image/jpeg;base64,' + imageData;
      let photo = this.dataURItoBlob(dataImg);
      let subidaTask = storageFilePath.put(photo);
      subidaTask.on('state_changed', (snapshot: firebase.storage.UploadTaskSnapshot) => {
        this.subiendo = true;
        this.loadProgress = ((snapshot.bytesTransferred / snapshot.totalBytes) * 100).toFixed(2);
      }, (error) => {

      }, () => {
        this.subidaCompletada = true;
        this.subiendo = false;
        let toastSuccesImg = this._toastCtrl.create({
          message: 'Imagen subida con éxito',
          duration: 3000
        }).present();
        firebase.auth().currentUser.updateProfile({
          displayName: firebase.auth().currentUser.displayName,
          photoURL: subidaTask.snapshot.downloadURL
        });
      });
    });
  }


  dataURItoBlob(dataURI) {
    // code adapted from: http://stackoverflow.com/questions/33486352/cant-upload-image-to-aws-s3-from-ionic-camera
    let binary = atob(dataURI.split(',')[1]);
    let array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
  };

  insertDamage(damage: DamageBase) {
    let userID: string = damage.userID;
    let fbListRef: AngularFireList<DamageBase> = this._afDB.list<DamageBase>(`damages`);
    let fbUserDamagesRef: AngularFireObject<any> = this._afDB.object<any>(`user_damages/${userID}`);
    fbListRef.push(damage).then((result) => {
      let key = result.key;
      fbUserDamagesRef.update({
        [key]: true
      });
    });
  }
}

