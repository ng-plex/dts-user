import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Badge } from '@ionic-native/badge';

import { timer } from 'rxjs/observable/timer';

// Pages
import { HomePage } from '../../../pages/home/home';

// Services
import { AuthenticationService } from './../../core/authentication/shared/services/authentication/authentication.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  showSplash: boolean = true;
  auth$ = this._authServ.authUser$;
  rootPage: any = 'LoginPage';
  public badgePermission: boolean = false;

  constructor(
    platform: Platform,
    statusBar: StatusBar, splashScreen: SplashScreen,
    private _authServ: AuthenticationService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      timer(3000).subscribe(() => this.showSplash = false) // <-- hide animation

      this._authServ.currentUser$.subscribe((user) => {
        if (user) {
          this.rootPage = 'DamagesPage';
        } else {
          this.rootPage = 'RegisterPage';
        }
      })
    });
  }

  goToPage(event: string) {
    this.rootPage = event;
  }

  logout() {
    // TODO: Verify
    this.goToPage('LoginPage');
  }
}

