import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Core Feature Authentication
import { AuthenticationModule } from './authentication/authentication.module';
import { IonicModule } from 'ionic-angular';

// Shared Module
import { SharedModule } from './shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        AuthenticationModule,
        SharedModule,
    ],
    exports: [
        AuthenticationModule,
        SharedModule,
    ]
})
export class CoreModule { }
