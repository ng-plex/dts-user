import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Ionic Module
import { IonicModule } from 'ionic-angular/module';

// Services
import { LoadingService } from './services/loading.service';

// Shared Components
import { MainHeaderComponent } from './components/main-header/main-header.component';
import { SideMenuComponent } from './components/sidemenu/side-menu.component';
import { IonProgressBarComponent } from './components/ion-progress-bar/ion-progress-bar';
import { UploadComponent } from './components/upload/upload.component';

@NgModule({
    declarations: [
        SideMenuComponent,
        MainHeaderComponent,
        IonProgressBarComponent,
        UploadComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
    ],
    providers: [
        LoadingService
    ],
    exports: [
        SideMenuComponent,
        MainHeaderComponent,
        IonProgressBarComponent,
        UploadComponent,
    ],
})
export class SharedModule { }
