import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

import { IonProgressBarComponent } from './ion-progress-bar';

@NgModule({
    imports: [
        CommonModule,
        IonicModule
    ],
    declarations: [
        IonProgressBarComponent
    ],
    exports: [
        IonProgressBarComponent
    ]
})
export class IonProgressBarModule {

}