import { Component, Input } from '@angular/core';

@Component({
  selector: 'ion-progress-bar',
  templateUrl: 'ion-progress-bar.html'
})
export class IonProgressBarComponent {

  @Input('progress') progress: string;

  constructor() { }

}
