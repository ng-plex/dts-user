import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

// Authentication Service
import { AuthenticationService } from '../../../authentication/shared/services/authentication/authentication.service';
import { MenuController } from 'ionic-angular';

@Component({
    selector: 'side-menu',
    templateUrl: 'side-menu.component.html',
})

export class SideMenuComponent implements OnInit {
    @Input() content: any;
    @Input() user: any;

    @Output() logedOut = new EventEmitter<void>();

    @Output() onRootChanged = new EventEmitter<string>();

    auth$ = this._authServ.authUser$;

    constructor(
        private _authServ: AuthenticationService,
        private _menuCtrl: MenuController
    ) { }

    ngOnInit() { }

    setRoot(page: string) {
        this.onRootChanged.emit(page);
    }

    doLogout() {
        this.setRoot('LoginPage');
        this._menuCtrl.close();
        return this._authServ.logout();
    }
}