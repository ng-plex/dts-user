import { Component, Input, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

// Camera Plugin
import {
    Camera,
    CameraOptions,
    DestinationType,
    EncodingType,
    MediaType,
    PictureSourceType
} from '@ionic-native/camera';

// AngularFire2 Storage
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';

// Default Params
export const DEFAULT_CAMERA_OPTIONS: CameraOptions = {
    quality: 100,
    destinationType: DestinationType.DATA_URL,
    encodingType: EncodingType.JPEG,
    mediaType: MediaType.PICTURE,
    sourceType: PictureSourceType.CAMERA
};

@Component({
    selector: 'upload-component',
    templateUrl: 'upload.component.html',
})
export class UploadComponent {
    @Output() saved: EventEmitter<Observable<string>> = new EventEmitter();

    uploadTask: AngularFireUploadTask //DOWNLOADURL
    uploadProgress: any;
    image;
    base64Image: string;
    error;
    url: Observable<string>;
    //uploading: boolean = false;

    constructor(
        private _camera: Camera,
        private _afStorage: AngularFireStorage
    ) { }

    public takePhoto() {
        return this._camera.getPicture(DEFAULT_CAMERA_OPTIONS)
    }

    public createUploadTask(file: string): void {
        // Esta parte sería la captura de la foto y el establecimiento del nombre del archivo
        const filePath = `dts_images/damage_${ new Date().getTime() }.jpg`;
        let image64 = 'data:image/jpg;base64,' + file;
        this.image = image64;

        // Dividir desde aquí para el handleUpload()
        this.uploadTask = this._afStorage.ref(filePath).putString(image64, 'data_url');
        this.uploadTask.percentageChanges().subscribe((percent) => this.uploadProgress = percent.toFixed(2));
        this.url = this.uploadTask.downloadURL();
    }

    public async handleUpload() {
        this.base64Image = await this.takePhoto();
        this.createUploadTask(this.base64Image);
        this.saved.emit(this.url);
    }

    public storePhotoDB() {
        
    }
}