import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'main-header',
    templateUrl: 'main-header.component.html'
})

export class MainHeaderComponent implements OnInit {
    @Input() title: string;
    @Input() headerColor: string;

    constructor() { }

    ngOnInit() { }
}