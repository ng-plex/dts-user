import { Injectable } from '@angular/core';

// Loading Controller
import { LoadingController, Loading, LoadingOptions } from 'ionic-angular';

// Loading options
const LOADING_DEFAULT_MESSAGE = `Por favor, espere. Estamos procesando su solicitud. Gracias`;
const DEFAULT_DURATION = 2000;
const DEFAULT_SPINNER = 'circles';

@Injectable()
export class LoadingService {
    private _loader: Loading;

    constructor(
        private loadingCtrl: LoadingController
    ) { }

    /**
     * Muestra un mensaje de carga
     * 
     * @param opts 
     * @returns Promise<any>
     */
    showLoading(opts?: LoadingOptions) {
        let options = Object.assign({}, { spinner: DEFAULT_SPINNER, duration: DEFAULT_DURATION, content: LOADING_DEFAULT_MESSAGE }, opts);
        this._loader = this.loadingCtrl.create(options);
        return this._loader.present();
    }


    /**
     * Cierra el mensaje de carga
     * 
     * @return Promise<any>
     */
    closeLoading() {
        return this._loader.dismiss();
    }
}