import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NavController } from 'ionic-angular';

// Authentication Service
import { AuthenticationService } from '../../shared/services/authentication/authentication.service';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { SettingsService } from '../../../../profile/services/settings/settings.service';

@Component({
    selector: 'register-component',
    templateUrl: 'register.component.html'
})

export class RegisterComponent implements OnInit {
    error: string;
    constructor(
        private _authServ: AuthenticationService,
        private _navCtrl: NavController,
        private _toastCtrl: ToastController
    ) { }

    ngOnInit() { }

    registerUser(event: FormGroup) {
        const { username, email, password } = event.value;
        try {
            // It returns the User (the param used in the chainable Promise)
            this._authServ.createUser(username, email, password)
                .then(() => {
                    this._navCtrl.setRoot('DamagesPage')
                    let toastSuccess = this._toastCtrl.create({
                        message: `Usuario ${email} creado satisfactoriamente`,
                        duration: 3000
                    });
                    toastSuccess.present();
                })
                .catch(err => {
                    let toastError = this._toastCtrl.create({
                        message: err.message,
                        duration: 5000
                    });
                    toastError.present();
                });
        } catch (error) {
            this.error = error.message;
        }
    }

    goToLogin() {
        this._navCtrl.setRoot('LoginPage');
    }
}