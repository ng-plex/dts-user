import { Component, OnInit } from '@angular/core';

import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { NavController } from 'ionic-angular';

// Authentication Service
import { AuthenticationService } from '../../shared/services/authentication/authentication.service';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'login-component',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    
    error: string;
    constructor(
        private _authServ: AuthenticationService,
        private _navCtrl: NavController,
        private _toastCtrl: ToastController
    ) { }

    ngOnInit() {

    }

    loginUser(event: FormGroup) {
        const { email, password } = event.value;
        try {
            // It returns the User (the param used in the chainable Promise)
            this._authServ.loginUser(email, password)
                .then(() => {
                    this._navCtrl.setRoot('DamagesPage')
                    let toastSuccess = this._toastCtrl.create({
                        message: `Accediendo al Sistema`,
                        duration: 1500
                    });
                    toastSuccess.present();
                })
                .catch(err => {
                    let toastError = this._toastCtrl.create({
                        message: err.message,
                        duration: 5000
                    });
                    toastError.present();
                });
        } catch (error) {
            this.error = error.message;
        }
    }

    goToRegister() {
        this._navCtrl.setRoot('RegisterPage');
    }
}