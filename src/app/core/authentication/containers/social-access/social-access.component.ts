import { Component } from '@angular/core';

import { AuthenticationService } from '../../shared/services/authentication/authentication.service';

@Component({
    selector: 'social-access',
    templateUrl: 'social-access.component.html'
})
export class SocialAccessComponent {
    user: any;
    constructor(
        private _authServ: AuthenticationService
    ) {
        this.user = this._authServ.currentUser$;
    }

    doFacebookLogin() { }

    async doGoogleLogin() {
        await this._authServ.googleLogin();        
    }

    async doTwitterLogin() {
        await this._authServ.twitterLogin();
    }
}