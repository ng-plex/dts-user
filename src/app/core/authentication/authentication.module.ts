import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

// Containers
import { LoginComponent } from './containers/login/login.component';
import { RegisterComponent } from './containers/register/register.component';
import { SocialAccessComponent } from './containers/social-access/social-access.component';


// Shared Authentication Module
import { SharedModule } from './shared/shared.module';

//Third-party Packages
// AngularFire2 and Firebase
import { environment } from '../../../env/environment.dev';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';
import { ProfileModule } from '../../profile/profile.module';

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        SocialAccessComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
        ProfileModule,
        SharedModule.forRoot(),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFireDatabaseModule
    ],
    exports: [
        LoginComponent,
        RegisterComponent,
        SocialAccessComponent,
    ]
})
export class AuthenticationModule { }
