import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'login-form',
    templateUrl: 'login-form.component.html'
})

export class LoginFormComponent {

    // Property for show or not the password field value
    showPWD: boolean = false;

    // Password field type
    passwordFieldType: string = 'password';

    @Output() movedToRegister = new EventEmitter<void>();
    @Output() submitted = new EventEmitter<FormGroup>();

    form: FormGroup = this._fb.group({
        // TODO: Cambiar para producción
        email: ['', Validators.email],
        password: ['', Validators.required]
    });


    constructor(private _fb: FormBuilder) { }

    showPassword() {
        this.showPWD = !this.showPWD;

        (this.showPWD) ? this.passwordFieldType = 'text' : this.passwordFieldType = 'password';

    }

    onSubmit() {
        if (this.form.valid) {
            this.submitted.emit(this.form);
        }
    }

    // Errors and Form Controls State

    get emailFormat() {
        const control = this.form.get('email');
        return control.hasError('email')
            && control.touched;
    }
    get passwordInvalid() {
        const control = this.form.get('password');
        return control.hasError('required')
            && control.touched;
    }

    goToRegister() {
        this.movedToRegister.emit();
    }

}