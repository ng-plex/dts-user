import { Component, Output, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'register-form',
    templateUrl: 'register-form.component.html',
})

export class RegisterFormComponent {

    @Output() movedToLogin = new EventEmitter<void>();
    @Output() submitted = new EventEmitter<FormGroup>();


    form: FormGroup = this._fb.group({
        username: ['', Validators.required],
        email: ['', Validators.email],
        password: ['', Validators.required]
    });;


    constructor(private _fb: FormBuilder) { }

    onSubmit() {
        if (this.form.valid) {
            this.submitted.emit(this.form);
        }
    }

    // Errors and Form Controls State

    get usernameInvalid() {
        const control = this.form.get('username');
        return control.hasError('required')
            && control.touched;
    }
    get emailFormat() {
        const control = this.form.get('email');
        return control.hasError('email')
            && control.touched;
    }
    get passwordInvalid() {
        const control = this.form.get('password');
        return control.hasError('required')
            && control.touched;
    }

    goToLogin() {
        this.movedToLogin.emit();
    }
}