import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Ionic Directives for Custom Components
import { IonicModule } from 'ionic-angular';

// Components
import { LoginFormComponent } from './components/login-form/login-form.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';

// Services
import { AuthenticationService } from './services/authentication/authentication.service';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
    ],
    declarations: [
        LoginFormComponent,
        RegisterFormComponent
    ],
    exports: [
        LoginFormComponent,
        RegisterFormComponent,
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [AuthenticationService]
        };
    }
}
