import { Injectable } from '@angular/core';

// AngularFire2 and Firebase dependencies
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { SettingsService } from '../../../../../profile/services/settings/settings.service';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from '../../models/user.model';

// Default Profile Photo
export const DEFAULT_PHOTO = '../assets/imgs/no-photo_white.svg';

import { ToastController } from 'ionic-angular';

@Injectable()
export class AuthenticationService {
    public isLoggedIn: boolean = false;
    _authUserSubject: BehaviorSubject<User> = new BehaviorSubject(null);
    currentUser$ = this._authUserSubject.asObservable();

    constructor(
        private _afAuth: AngularFireAuth,
        private _afDb: AngularFireDatabase,
        private _toastCtrl: ToastController,
        private _settingsServ: SettingsService) {

        this._afAuth.authState.subscribe((auth) => {
            this.getAuthenticatedUser(auth);
        })

    }

    getAuthenticatedUser(auth: firebase.User) {
        if (auth) {
            let user: User = {
                uid: auth.uid,
                displayName: auth.displayName,
                email: auth.email,
                photoURL: auth.photoURL
            }
            this._authUserSubject.next(user);
        };
    }

    async createUser(username: string, email: string, password: string) {
        let user = await this._afAuth.auth.createUserWithEmailAndPassword(email, password);
        this.updateProfile(user, username);
        this._settingsServ.save(user);
        this.isLoggedIn = true;
    }

    async loginUser(email: string, password: string) {
        this.isLoggedIn = true;
        let user = await this._afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    async logout() {
        await this._afAuth.auth.signOut();
        this.isLoggedIn = false;
    }

    private updateProfileDb(user: firebase.User) {
        let { uid, displayName, email } = user;
        return this._afDb.object(`users/${user.uid}`).set({
            uid,
            displayName,
            email
        });
    }

    // Update the Profile user based on the User entered data
    public updateProfile(user: firebase.User, username: string, photoURL = DEFAULT_PHOTO) {
        user.updateProfile({
            displayName: username,
            photoURL: user.photoURL || photoURL
        })
            .then((res) => this.updateProfileDb(this._afAuth.auth.currentUser)); //Update the user profile in the DB
    }

    get authUser$() {
        return this._afAuth.authState;
    }

    public facebookLogin() {

    }

    public twitterLogin() {
        return this._afAuth.auth.signInWithRedirect(new firebase.auth.TwitterAuthProvider())
            .then((result) => {
                //this.updateProfile(result.user, result.user.displayName);
                this.updateProfileDb(this._afAuth.auth.currentUser);
                this._settingsServ.save(result.user);
                this.isLoggedIn = true;
                let toast = this._toastCtrl.create({
                    message: 'Usuario de Twiter Registrado satisfactoriamente.',
                    duration: 3000,
                    showCloseButton: true
                });
            })
            .catch((error) => {
                if (error.code === 'auth/account-exists-with-different-credential' || error.code === 'auth/credential-already-in-use') {
                    let toast = this._toastCtrl.create({
                        message: 'La cuenta ya está asociada a un usuario, por favor, usa el método usado con anterioridad. Gracias',
                        duration: 3000,
                        showCloseButton: true
                    });
                    toast.present();
                }
            });
    }

    public googleLogin() {
        // let provider = new firebase.auth.GoogleAuthProvider();
        // provider.addScope('email');
        return this._afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider()).then((result) => {
            //this.updateProfile(result.user, result.user.displayName);
            this.updateProfileDb(this._afAuth.auth.currentUser);
            this._settingsServ.save(result.user);
            this.isLoggedIn = true;
        }).catch((error) => {
            if (error.code === 'auth/account-exists-with-different-credential' || error.code === 'auth/credential-already-in-use') {
                let toast = this._toastCtrl.create({
                    message: 'La cuenta ya está asociada a un usuario, por favor, usa el método usado con anterioridad. Gracias',
                    duration: 3000,
                    showCloseButton: true
                });
                toast.present();
            }
        });
    }


}