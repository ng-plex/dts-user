export interface User {
    uid: string;
    displayName: string;
    email: string;
    photoURL: string;
}

export interface Credentials {
    username?: string;
    email?: string;
    password?: string;
}